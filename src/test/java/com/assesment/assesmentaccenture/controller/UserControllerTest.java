package com.assesment.assesmentaccenture.controller;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.23
 */

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ActiveProfiles("userControllerTest")
@SpringBootTest
public class UserControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Before
    @Test
    public void postUserValid() throws Exception {
        JSONObject payload = new JSONObject();
        payload.put("name", "Ardi");
        payload.put("socialSecurityNumber", (Math.random() * 1000));
        payload.put("dateOfBirth", "2020-08-10");


        RequestBuilder builder = MockMvcRequestBuilders
                .post("/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload.toString());

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(201, result.getResponse().getStatus());
    }

    @Test
    public void postUserInvalidRequest() throws Exception {
        JSONObject payload = new JSONObject();
        payload.put("namee", "Ardi");
        payload.put("socialSecurityNumberr", (Math.random() * 1000));
        payload.put("dateOfBirthh", "2020-08-10");


        RequestBuilder builder = MockMvcRequestBuilders
                .post("/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload.toString());

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    @Test
    public void postUserCatch() throws Exception {
        JSONObject payload = new JSONObject();
        payload.put("name", "Ardi");
        payload.put("socialSecurityNumber", "abc");
        payload.put("dateOfBirth", "2020-08-10");


        RequestBuilder builder = MockMvcRequestBuilders
                .post("/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload.toString());

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(400, result.getResponse().getStatus());
    }

    /*
     * PUT /v1/users/{id}
     * */
    @Test
    public void putUserValid() throws Exception {
        JSONObject payload = new JSONObject();
        payload.put("name", "Ardi");
        payload.put("socialSecurityNumber", (Math.random() * 1000));
        payload.put("dateOfBirth", "2020-08-10");
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload.toString());

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void putUserInvalid() throws Exception {
        JSONObject payload = new JSONObject();
        payload.put("name", "Ardi");
        payload.put("socialSecurityNumber", new Random().nextLong());
        payload.put("dateOfBirth", "2020-08-10");
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/v1/users/"+ new Random().nextLong())
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload.toString());

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    /*
     * GET /v1/users
     * */
    @Test
    public void getAllUsersNotEmpty() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/v1/users")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();
        JSONObject jsonObject = new JSONObject(result.getResponse().getContentAsString());

        assertEquals(200, result.getResponse().getStatus());
        assertEquals(true, jsonObject.get("data").toString().length() > 0);
    }
    /*
     * GET /v1/users/{id}
     * */
    @Test
    public void getUserFound() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
    }
    @Test
    public void getUserNotFound() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/v1/users/"+ new Random().nextLong())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    /*
     * DELETE /v1/users/{id}
     * */
    @Test
    public void deleteUserFound() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders
                .delete("/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void deleteUserNotFound() throws Exception {
        RequestBuilder builder = MockMvcRequestBuilders
                .delete("/v1/users/"+ new Random().nextLong())
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc
                .perform(builder)
                .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }
}