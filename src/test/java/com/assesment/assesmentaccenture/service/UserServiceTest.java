package com.assesment.assesmentaccenture.service;

import com.assesment.assesmentaccenture.entity.UserEntity;
import com.assesment.assesmentaccenture.entity.dto.UserDto;
import com.assesment.assesmentaccenture.repository.UserRepository;
import com.assesment.assesmentaccenture.util.CommonResponse;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.21
 */

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ActiveProfiles("userServiceTest")
@SpringBootTest
//@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userServiceImple;

    UserDto userDto = UserDto.builder()
            .name("Ardi")
            .socialSecurityNumber(12345)
            .dateOfBirth("2020-08-10")
            .build();

    UserEntity userEntity = UserEntity.builder()
            .id(1L)
            .isDeleted(false)
            .name("Ardi")
            .socialSecurityNumber(12345)
            .dateOfBirth(LocalDate.parse("2020-08-10"))
            .build();

    /*
     * POST /v1/user
     * */
    @Test
    public void saveValid() throws Exception {
        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(0, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidNameLessTwo() throws Exception {
        userDto.setName("A");

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidSsn() throws Exception {
        UserDto userDto = UserDto.builder()
                .name("Ardi")
                .socialSecurityNumber(123451)
                .dateOfBirth("2020-08-10")
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidSsnLessFive() throws Exception {
        UserDto userDto = UserDto.builder()
                .name("Ardi")
                .socialSecurityNumber(1)
                .dateOfBirth("2020-08-10")
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(0, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidNameNull() throws Exception {
        UserDto userDto = UserDto.builder()
                .name(null)
                .socialSecurityNumber(123451)
                .dateOfBirth("2020-08-10")
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidDateOfBirthIsNull() throws Exception {
        UserDto userDto = UserDto.builder()
                .name("Ardi")
                .socialSecurityNumber(23451)
                .dateOfBirth(null)
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveInvalidDateOfFormat() throws Exception {
        UserDto userDto = UserDto.builder()
                .name("Ardi")
                .socialSecurityNumber(23451)
                .dateOfBirth("10-05-2020")
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void saveHandleExeption() throws Exception {
        UserDto userDto = UserDto.builder()
                .name(null)
                .socialSecurityNumber(23451)
                .dateOfBirth("10-05-2020")
                .build();

        CommonResponse commonResponse = userServiceImple.saveUser(userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    /*
     * PUT /v1/user/{id}
     * */
    @Test
    public void updateValid() throws Exception {
        userDto.setSocialSecurityNumber(36985);
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(0, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidNameIsNull() throws Exception {
        userDto.setName("");
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidNameLessThenTwo() throws Exception {
        userDto.setName("A");
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidSsn() throws Exception {
        userDto.setSocialSecurityNumber(123456);
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidSsnLessFive() throws Exception {
        userDto.setSocialSecurityNumber(12);
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(0, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidDateOfBirthIsNull() throws Exception {
        userDto.setDateOfBirth(null);
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void updateInvalidDateOfFormat() throws Exception {
        userDto.setDateOfBirth("10-05-2020");
        given(userRepository.findByIdAndIsDeletedFalse(userEntity.getId())).willReturn(Optional.of(userEntity));

        CommonResponse commonResponse = userServiceImple.updateUser(userEntity.getId(), userDto);
        assertEquals(30001, commonResponse.getResponseSchema().getCode());
    }

    @Test
    public void getAllUsers() throws Exception {

        List<UserEntity> list = userServiceImple.listUserActive();
        assertThat(list).isNotNull();
    }


}
