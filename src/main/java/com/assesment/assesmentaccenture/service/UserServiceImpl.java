package com.assesment.assesmentaccenture.service;

import com.assesment.assesmentaccenture.entity.UserEntity;
import com.assesment.assesmentaccenture.entity.dto.UserDto;
import com.assesment.assesmentaccenture.repository.UserRepository;
import com.assesment.assesmentaccenture.util.CommonResponse;
import com.assesment.assesmentaccenture.util.ResponseCodeMessage;
import com.assesment.assesmentaccenture.util.ResponseSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.15
 */


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public CommonResponse saveUser(UserDto userDto) {
        CommonResponse result = new CommonResponse();
        ResponseSchema resultResponeSchema = new ResponseSchema();
        UserEntity userEntity = new UserEntity();

        // check of lenght of social security number is equal to 5 digits
        String socialSecurityNumberString = String.valueOf(userDto.getSocialSecurityNumber());
        int socialSecurityNumberLength = socialSecurityNumberString.length();

        if (userDto.getName() == null || userDto.getName().isEmpty()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "name" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "null");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (userDto.getName().length() < 2) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "name" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "length < 2");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (socialSecurityNumberLength == 0 || socialSecurityNumberLength > 5) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "socialSecurityNumber" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + userDto.getSocialSecurityNumber());
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (userDto.getDateOfBirth() == null) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "dateOfBirth" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "null");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }

        // convert string to local date
        try {
            LocalDate dateOfBirth = LocalDate.parse(userDto.getDateOfBirth(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            userEntity.setDateOfBirth(dateOfBirth);
        } catch (Exception e) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "dateOfBirth" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + userDto.getDateOfBirth());
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }

        // add 0 to the left of social security number if it is less than 5 digits
        if (socialSecurityNumberLength < 5) {
            while (socialSecurityNumberLength < 5) {
                socialSecurityNumberString = socialSecurityNumberString + "0";
                socialSecurityNumberLength = socialSecurityNumberString.length();
            }
        }
        userDto.setSocialSecurityNumber(Long.parseLong(socialSecurityNumberString));

        // check if the social security number is already exist
        Optional<UserEntity> ssnCheck = userRepository.findBySocialSecurityNumber(userDto.getSocialSecurityNumber());

        if (ssnCheck.isPresent()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_CONFLICT);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_CONFLICT_A + userDto.getSocialSecurityNumber() + ResponseCodeMessage.MESSAGE_CONFLICT_B);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_CONFLICT);
            result.setResponseSchema(resultResponeSchema);
        } else {
            userEntity.setName(userDto.getName());
            userEntity.setSocialSecurityNumber(userDto.getSocialSecurityNumber());
            userRepository.save(userEntity);
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_CREATED);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_CREATED);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_CREATED);
            result.setResponseSchema(resultResponeSchema);
        }

        return result;
    }

    @Override
    public CommonResponse updateUser(Long id, UserDto userDto) {
        CommonResponse result = new CommonResponse();
        ResponseSchema resultResponeSchema = new ResponseSchema();
        UserEntity userEntity = new UserEntity();

        // check id is available or not
        Optional<UserEntity> userEntityCheck = userRepository.findByIdAndIsDeletedFalse(id);
        if (!userEntityCheck.isPresent()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_NOT_FOUND);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_NOT_FOUND + id);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_NOT_FOUND);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }

        // check of lenght of social security number is equal to 5 digits
        String socialSecurityNumberString = String.valueOf(userDto.getSocialSecurityNumber());
        int socialSecurityNumberLength = socialSecurityNumberString.length();

        if (userDto.getName() == null || userDto.getName().isEmpty()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "name" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "null");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (userDto.getName().length() < 2) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "name" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "length < 2");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (socialSecurityNumberLength == 0 || socialSecurityNumberLength > 5) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "socialSecurityNumber" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + userDto.getSocialSecurityNumber());
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        } else if (userDto.getDateOfBirth() == null) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "dateOfBirth" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + "null");
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }
        // add 0 to the left of social security number if it is less than 5 digits
        if (socialSecurityNumberLength < 5) {
            while (socialSecurityNumberLength < 5) {
                socialSecurityNumberString = socialSecurityNumberString + "0";
                socialSecurityNumberLength = socialSecurityNumberString.length();
            }
        }

        userDto.setSocialSecurityNumber(Long.parseLong(socialSecurityNumberString));

        // convert string to local date
        try {
            LocalDate dateOfBirth = LocalDate.parse(userDto.getDateOfBirth(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            userEntity.setDateOfBirth(dateOfBirth);
        } catch (Exception e) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_BAD_REQUEST);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_BAD_REQUEST_A + "dateOfBirth" + ResponseCodeMessage.MESSAGE_BAD_REQUEST_B + userDto.getDateOfBirth());
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_BAD_REQUEST);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }

        // check if the social security number is already exist
        Optional<UserEntity> ssnCheck = userRepository.findBySocialSecurityNumber(userDto.getSocialSecurityNumber());

        if (ssnCheck.isPresent() && ssnCheck.get().getId() != id) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_CONFLICT);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_CONFLICT_A + userDto.getSocialSecurityNumber() + ResponseCodeMessage.MESSAGE_CONFLICT_B);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_CONFLICT);
            result.setResponseSchema(resultResponeSchema);
        } else {
            userEntity.setId(id);
            userEntity.setName(userDto.getName());
            userEntity.setSocialSecurityNumber(userDto.getSocialSecurityNumber());
            userRepository.save(userEntity);
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_CREATED);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_CREATED);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_CREATED);
            result.setResponseSchema(resultResponeSchema);
        }
        return result;
    }

    @Override
    public List<UserEntity> listUserActive() {
        return userRepository.findByIsDeletedFalse();
    }

    @Override
    public CommonResponse findByIdAndIsDeletedFalse(Long id) {
        CommonResponse result = new CommonResponse();
        ResponseSchema resultResponeSchema = new ResponseSchema();
        Optional<UserEntity> userEntityCheck = userRepository.findByIdAndIsDeletedFalse(id);
        if (!userEntityCheck.isPresent()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_NOT_FOUND);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_NOT_FOUND + id);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_NOT_FOUND);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }
        result.setResponseSchema(resultResponeSchema);
        return result;
    }

    @Override
    public CommonResponse deleteUser(Long id) {
        CommonResponse result = new CommonResponse();
        ResponseSchema resultResponeSchema = new ResponseSchema();
        Optional<UserEntity> userEntityCheck = userRepository.findByIdAndIsDeletedFalse(id);
        if (!userEntityCheck.isPresent()) {
            resultResponeSchema.setStatus(ResponseCodeMessage.STATUS_NOT_FOUND);
            resultResponeSchema.setMessage(ResponseCodeMessage.MESSAGE_NOT_FOUND + id);
            resultResponeSchema.setCode(ResponseCodeMessage.CODE_NOT_FOUND);
            result.setResponseSchema(resultResponeSchema);
            return result;
        }
        result.setResponseSchema(resultResponeSchema);
        userEntityCheck.get().setDeleted(true);
        userRepository.save(userEntityCheck.get());
        return result;
    }
}
