package com.assesment.assesmentaccenture.service;

import com.assesment.assesmentaccenture.entity.UserEntity;
import com.assesment.assesmentaccenture.entity.dto.UserDto;
import com.assesment.assesmentaccenture.util.CommonResponse;

import java.text.ParseException;
import java.util.List;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 09.57
 */

public interface UserService {
    CommonResponse saveUser(UserDto userDto) throws ParseException;

    CommonResponse updateUser(Long id, UserDto userDto);

    CommonResponse findByIdAndIsDeletedFalse(Long id);

    List<UserEntity> listUserActive();

    CommonResponse deleteUser(Long id);
}
