package com.assesment.assesmentaccenture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssesmentAccentureApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssesmentAccentureApplication.class, args);
	}

}
