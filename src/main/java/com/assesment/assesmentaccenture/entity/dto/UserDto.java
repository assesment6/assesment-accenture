package com.assesment.assesmentaccenture.entity.dto;


import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.14
 */


@Data
@Getter
@Setter
@Builder
public class UserDto {
 private String name;
 private long socialSecurityNumber;
 private String dateOfBirth;
}
