package com.assesment.assesmentaccenture.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLInsert;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.13
 */

@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "social_security_number", nullable = false, unique = true)
    private long socialSecurityNumber;

    @Column(name = "date_of_birth", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "created", nullable = false, updatable = false)
    private LocalDateTime created = LocalDateTime.now();

    @Column(name = "updated")
    @UpdateTimestamp
    private Instant updated;

    @Column(name = "updated_by", nullable = false)
    private String updatedBy = "0";

    @Column(name = "created_by", nullable = false)
    private String createdBy = "0";

    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted = false;
}