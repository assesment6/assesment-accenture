package com.assesment.assesmentaccenture.repository;

import com.assesment.assesmentaccenture.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.16
 */

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findBySocialSecurityNumber(long parseLong);

    Optional<UserEntity> findByIdAndIsDeletedFalse(Long id);

    List<UserEntity> findByIsDeletedFalse();
}