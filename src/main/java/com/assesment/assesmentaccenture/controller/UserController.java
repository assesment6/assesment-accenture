package com.assesment.assesmentaccenture.controller;

import com.assesment.assesmentaccenture.entity.UserEntity;
import com.assesment.assesmentaccenture.entity.dto.UserDto;
import com.assesment.assesmentaccenture.service.UserService;
import com.assesment.assesmentaccenture.util.CommonResponse;
import com.assesment.assesmentaccenture.util.ResponseCodeMessage;
import com.assesment.assesmentaccenture.util.ResponseSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * Created by,
 * MAS, Wednesday, 10/08/22
 * 15.46
 */
@RestController
@RequestMapping("/v1/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public ResponseEntity<ResponseSchema> createUser(@RequestBody UserDto userDto) throws ParseException {
        CommonResponse dataInsert = userService.saveUser(userDto);

        if (dataInsert.getResponseSchema().getCode() > 0) {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataInsert.getResponseSchema().getCode(),
                            dataInsert.getResponseSchema().getStatus(),
                            dataInsert.getResponseSchema().getMessage()
                    ),
                    dataInsert.getResponseSchema().getCode() == 30002 ?
                            HttpStatus.CONFLICT
                            :
                            dataInsert.getResponseSchema().getCode() == 30001 ?
                                    HttpStatus.BAD_REQUEST
                                    :
                                    HttpStatus.CONFLICT
            );
        } else {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            userDto
                    ),
                    HttpStatus.CREATED
            );
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseSchema> updateUser(@PathVariable("id") Long id, @RequestBody UserDto userDto) throws ParseException {
        CommonResponse dataUpdate = userService.updateUser(id, userDto);

        if (dataUpdate.getResponseSchema().getCode() > 0) {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataUpdate.getResponseSchema().getCode(),
                            dataUpdate.getResponseSchema().getStatus(),
                            dataUpdate.getResponseSchema().getMessage()
                    ),
                    HttpStatus.NOT_FOUND
            );
        } else {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            userDto
                    ),
                    HttpStatus.OK
            );
        }
    }

    @GetMapping("")
    public ResponseEntity<ResponseSchema> listUser() {
        List<UserEntity> userDtoList = userService.listUserActive();
        if (userDtoList.isEmpty()) {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            ResponseCodeMessage.CODE_NOT_FOUND,
                            ResponseCodeMessage.STATUS_NOT_FOUND,
                            ResponseCodeMessage.MESSAGE_NOT_FOUND_LIST
                    ),
                    HttpStatus.NOT_FOUND
            );
        } else {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            userDtoList
                    ),
                    HttpStatus.OK
            );
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseSchema> getUserById(@PathVariable("id") Long id) {
        CommonResponse dataGet = userService.findByIdAndIsDeletedFalse(id);
        if (dataGet.getResponseSchema().getCode() > 0) {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataGet.getResponseSchema().getCode(),
                            dataGet.getResponseSchema().getStatus(),
                            dataGet.getResponseSchema().getMessage()
                    ),
                    HttpStatus.NOT_FOUND
            );
        } else {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataGet.getResponseSchema().getData()
                    ),
                    HttpStatus.OK
            );
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseSchema> deleteUser(@PathVariable("id") Long id) {
        CommonResponse dataDelete = userService.deleteUser(id);
        if (dataDelete.getResponseSchema().getCode() > 0) {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataDelete.getResponseSchema().getCode(),
                            dataDelete.getResponseSchema().getStatus(),
                            dataDelete.getResponseSchema().getMessage()
                    ),
                    HttpStatus.NOT_FOUND
            );
        } else {
            return new ResponseEntity<ResponseSchema>(
                    new ResponseSchema(
                            dataDelete.getResponseSchema().getData()
                    ),
                    HttpStatus.OK
            );
        }
    }

}