package com.assesment.assesmentaccenture.util;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 10.12
 */

public class ResponseSchema {
 private String status;
 private long code;
 private String message;
 private Object data;

 public ResponseSchema() {
 }

 public ResponseSchema(Object data) {
  this.data = data;
 }

 public ResponseSchema(long code, String status, String message) {
  this.code = code;
  this.status = status;
  this.message = message;
 }

 public String getStatus() {
  return status;
 }

 public void setStatus(String status) {
  this.status = status;
 }

 public long getCode() {
  return code;
 }

 public void setCode(long code) {
  this.code = code;
 }

 public String getMessage() {
  return message;
 }

 public void setMessage(String message) {
  this.message = message;
 }

 public Object getData() {
  return data;
 }

 public void setData(Object data) {
  this.data = data;
 }
}
