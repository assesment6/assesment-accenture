package com.assesment.assesmentaccenture.util;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 09.59
 */

public class CommonResponse {
 private ResponseSchema responseSchema;
 private Object data;

 public CommonResponse(long code, String status, String message, Object data) {
  ResponseSchema responseSchema = new ResponseSchema();
  responseSchema.setCode(code);
  responseSchema.setStatus(status);
  responseSchema.setMessage(message);
  this.responseSchema = responseSchema;
  this.data = data;
 }

 public CommonResponse() {

 }

 public ResponseSchema getResponseSchema() {
  return responseSchema;
 }

 public void setResponseSchema(ResponseSchema responseSchema) {
  this.responseSchema = responseSchema;
 }

 public Object getData() {
  return data;
 }

 public void setData(Object data) {
  this.data = data;
 }
}
