package com.assesment.assesmentaccenture.util;

/**
 * Created by,
 * MAS, Thursday, 11/08/22
 * 09.59
 */

public class ResponseCodeMessage {
    public static final long CODE_CREATED = 0;
    public static final String STATUS_CREATED = "CREATED";
    public static final String MESSAGE_CREATED = "Successfully created";

    public static final long CODE_NOT_FOUND = 30000;
    public static final String STATUS_NOT_FOUND = "NOT_FOUND";
    public static final String MESSAGE_NOT_FOUND = "No such resource with id ";

    public static final long CODE_BAD_REQUEST = 30001;
    public static final String STATUS_BAD_REQUEST = "BAD_REQUEST";
    public static final String MESSAGE_BAD_REQUEST_A = "Invalid value for field ";
    public static final String MESSAGE_BAD_REQUEST_B = ", rejected value: ";


    public static final long CODE_CONFLICT = 30002;
    public static final String STATUS_CONFLICT = "CONFLICT";
    public static final String MESSAGE_CONFLICT_A = "Record with SSN ";
    public static final String MESSAGE_CONFLICT_B = " already exists in the system";


    public static final String MESSAGE_NAME_INVALID = "Name is required";
    public static final String MESSAGE_SOCIAL_SECURITY_NUMBER_INVALID = "Social Security Number is required";
    public static final String MESSAGE_DATE_OF_BIRTH_INVALID = "Date of Birth is required";
    public static final String MESSAGE_SOCIAL_SECURITY_NUMBER_EXIST = "Social Security Number is already exist";
    public static final String MESSAGE_NOT_FOUND_LIST = "Data not found";
}
